package com.faz;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

class DrawPanel extends DrawPanelAbstract {
    public DrawPanel(String ip, int port) {
        super(ip, port);
    }

    @Override
    protected void setConnectionSocket() {
        SocketAddress add = new InetSocketAddress(IP, PORT);
        ServerSocket ss = null;
        try {
            ss = new ServerSocket();
            ss.bind(add);
            socket = ss.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class DrawPanelCli extends  DrawPanelAbstract {
    public DrawPanelCli(String ip, int port) {
        super(ip, port);
    }

    @Override
    protected void setConnectionSocket() {
        SocketAddress add = new InetSocketAddress(IP, PORT);
        Socket cs = null;
        socket = new Socket();
        try {
            socket.connect(add);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

abstract class DrawPanelAbstract extends JPanel {
    public ArrayList<Integer[]> points = new ArrayList<Integer[]>();
    public volatile ArrayList<ArrayList<Integer[]>> selfLines = new ArrayList<ArrayList<Integer[]>>();
    public volatile ArrayList<ArrayList<Integer[]>> outerLines = new ArrayList<ArrayList<Integer[]>>();
    BufferedImage bi;
    Graphics big;
    Socket socket = null;
    ObjectOutputStream oos = null;
    ObjectInputStream ois = null;
    protected String IP = "127.0.0.1";
    protected int PORT = 3000;
    int SZ = 300;
    boolean pointsUpdated = true;

    public DrawPanelAbstract(String ip, int port) {

        IP = ip;
        PORT = port;
        bi = new BufferedImage(SZ, SZ, BufferedImage.TYPE_INT_ARGB);
        big = bi.getGraphics();
        big.setColor(Color.white);
        big.fillRect(0,0,SZ,SZ);
        setBackground(Color.white);
        selfLines.add(points);

        Thread netConnection = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    establishConnection();
                    System.out.println("accepted");
                    while(!socket.isClosed()) {
                        try {
                            if(socket!=null) {
                                outerLines = (ArrayList<ArrayList<Integer[]>>) ois.readObject();
                                updateUI();
                            } else {
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("Waiting OIS setting...");
                            }
                            //Thread.sleep(200);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        netConnection.start();

    }

    protected abstract void setConnectionSocket();

    public void establishConnection() throws IOException {
        setConnectionSocket();
        oos = new ObjectOutputStream(socket.getOutputStream());
        ois = new ObjectInputStream(socket.getInputStream());
    }

    public void undoLine() {
        if(selfLines.size() > 1) {
            points = selfLines.get(selfLines.size()-2);
            points.clear();
            selfLines.remove(selfLines.size()-1);
        }
        updateUI();
    }

    public void nextline() {
        if(points.size() > 1) {
            points = new ArrayList<Integer[]>();
            selfLines.add(points);
        } else {
            points.clear();
        }
    }

    public void sendLines() {
        System.out.println("sent");
        if(oos!=null) {
            try {
                //System.out.println("sent");
                oos.reset();
                oos.writeObject(selfLines);
//                for(ArrayList<Integer[]> lst:selfLines) {
//                    System.out.println(Arrays.deepToString(lst.toArray()));
//                }
                //System.out.println(Arrays.deepToString(points.toArray()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void doDrawing(Graphics g) {
        if(pointsUpdated) {
            sendLines();
            pointsUpdated = false;
        }
        g.setColor(Color.white);
        g.fillRect(0,0,SZ,SZ);

        g.setColor(Color.black);
        drawLines(g, selfLines);
        g.setColor(Color.red);
        drawLines(g, outerLines);
    }

    private void drawLines(Graphics g, ArrayList<ArrayList<Integer[]>> lines) {
        for(ArrayList<Integer[]> pts : lines) {
            if(pts.size()>1) {
                for(int i=1; i<pts.size(); i++) {
                    g.drawLine(pts.get(i-1)[0],pts.get(i-1)[1],pts.get(i)[0],pts.get(i)[1]);
                }
            }
        }
    }

//    private void doDrawing2(Graphics g) {
//        big.setColor(Color.black);
//        if(points.size()>1) {
//            for(int i=1; i<points.size(); i++) {
//                big.drawLine(points.get(i-1)[0],points.get(i-1)[1],points.get(i)[0],points.get(i)[1]);
//            }
//            Integer[] last = points.get(points.size()-1);
//            points.clear();
//            points.add(last);
//        }
//        System.out.println(Arrays.deepToString(points.toArray()));
//
//        g.drawImage(bi, 0, 0, null);
//    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }
}


public class NetworkPainter extends JFrame {
    private boolean isServer = false;
    DrawPanelAbstract dpnl;
    int SZ=300;

    public NetworkPainter() throws IOException {
        this("server_config.ini");
    }

    public NetworkPainter(String configfile) throws IOException {
        initUI(configfile);
        setVisible(true);
    }

    public final void initUI(String configfile) throws IOException {
        boolean server=false;
        String host="127.0.0.1";
        int hostport=3000;
        String localbind="127.0.0.1";
        int localbindport=3000;

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(configfile)));

        String line;
        while((line=br.readLine())!=null) {
            String[] res = line.split(":");
            System.out.println(res[0]);
            if (res[0].toLowerCase().trim().equals("server")) {
                if(res[1].trim().toLowerCase().equals("true")) {
                    server=true;
                } else {
                    server=false;
                }
            } else if (res[0].toLowerCase().trim().equals("host")) {
                res = res[1].split(",");
                host = res[0].toLowerCase().trim();
                hostport = Integer.parseInt(res[1].toLowerCase().trim());
            } else if (res[0].toLowerCase().trim().equals("localbind")) {
                res = res[1].split(",");
                localbind = res[0].toLowerCase().trim();
                localbindport = Integer.parseInt(res[1].toLowerCase().trim());
            }
        }
        if (server) {
            setTitle("Server");
            dpnl = new DrawPanel(localbind, localbindport);
        } else {
            setTitle("Client");
            dpnl = new DrawPanelCli(host, hostport);
        }

        dpnl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    dpnl.nextline();
                    dpnl.pointsUpdated = true;
                }
            }

            public void mousePressed(MouseEvent e) {
                if(SwingUtilities.isRightMouseButton(e)) {
                    dpnl.undoLine();
                    dpnl.pointsUpdated = true;
                }
            }
        });

        dpnl.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    dpnl.pointsUpdated = true;
                    dpnl.points.add(new Integer[]{e.getX(), e.getY()});
                    dpnl.updateUI();
                }
            }
        });
        add(dpnl);
        setSize(SZ+10, SZ+30);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if(args.length == 0) {
            new NetworkPainter("server_config.ini");
            Thread.sleep(100);
            new NetworkPainter("client_config.ini");
        } else if(args[0].equals("server")) {
            new NetworkPainter("server_config.ini");
        } else if(args[0].equals("client")) {
            new NetworkPainter("client_config.ini");
        } else {
            System.out.println("Invalid arguments");
        }
    }

}
